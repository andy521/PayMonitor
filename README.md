# PayMonitor

#### 介绍
线下支付宝扫码实时订单监控客户端
> 本客户端基于java swing 开发，实现了个人支付宝账户到账准实时监控，主要思路是定时扫描支付宝账户商家账户详细页面，抓取接口响应数据并解析，展示在系统列表页面，目前功能还比较简单，持续完善中...


#### 软件架构
软件架构说明
![系统类结构图](https://images.gitee.com/uploads/images/2020/0917/093222_d2dba6c4_121703.png "paymonitor.png")

#### 系统截图

![登录界面](https://images.gitee.com/uploads/images/2020/0607/214617_70e406b4_121703.png "屏幕截图.png")</br>
![登录成功](https://images.gitee.com/uploads/images/2020/0607/214702_96959dee_121703.png "屏幕截图.png")</br>
![监控开启](https://images.gitee.com/uploads/images/2020/0607/214731_76bfeb7e_121703.png "屏幕截图.png")</br>
![到账](https://images.gitee.com/uploads/images/2020/0607/214754_7ae33b17_121703.png "屏幕截图.png")</br>
![我的账单](https://images.gitee.com/uploads/images/2020/0915/152600_3ba0f823_121703.png "屏幕截图.png")

### 注意
   ![输入图片说明](https://images.gitee.com/uploads/images/2020/0730/165226_0d497580_121703.png "屏幕截图.png")

#### 更新记录
1：新增监控类型下拉选择，并动态渲染列表。
2：增加了我的账单页面监控。
#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)