package com.javacoo.function;

/**
 * <li></li>
 *
 * @author: duanyong@jccfc.com
 * @since: 2020/8/6 10:54
 */
public interface Effect<T> {
    void apply(T t);
}
