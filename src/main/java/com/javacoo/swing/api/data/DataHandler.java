package com.javacoo.swing.api.data;

import com.javacoo.xkernel.spi.Spi;

/**
 * 数据处理
 * <p>说明:</p>
 * <li></li>
 *
 * @Author DuanYong
 * @Since 2019/8/30 23:41
 * @Version 1.0
 */
@Spi("default")
public interface DataHandler<T> {
    /**
     * 处理
     * <p>说明:</p>
     * <li></li>
     * @Author DuanYong
     * @Since 2019/8/30 23:42
     * @Version 1.0
     * @Params data
     */
    void handle(final T data);
    /**
     * 获取需要监控的链接
     * <p>说明:</p>
     * <li></li>
     * @author DuanYong
     * @date 2020/9/5 15:37
     */
    String getMonitorUrl();
}
