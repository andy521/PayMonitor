package com.javacoo.swing.api.monitor;

import com.javacoo.xkernel.spi.Spi;

/**
 * 监控窗口
 * <p>说明:</p>
 * <li></li>
 *
 * @Author DuanYong
 * @Since 2019/9/1 10:26
 * @Version 1.0
 */
@Spi("default")
public interface MonitorWindow {
    /**
     * 启动监控
     * <p>说明:</p>
     * <li></li>
     * @Author DuanYong
     * @Since 2019/9/1 10:34
     * @Version 1.0
     */
    void start();
}
