package com.javacoo.swing.core.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetAddress;
import java.net.UnknownHostException;


/**
 * IP工具类
 * <p>说明:</p>
 * <li></li>
 * @author DuanYong
 * @since 2018年3月6日上午9:12:43
 */
public class IPUtils {
	public static String getIpAddr() {
		String ip = "";
		InetAddress inet = null;
		try { // 根据网卡取本机配置的IP
			inet = InetAddress.getLocalHost();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		if(inet != null){
			ip = inet.getHostAddress();
		}
		return ip;
	}

}
