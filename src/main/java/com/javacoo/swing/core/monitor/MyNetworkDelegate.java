package com.javacoo.swing.core.monitor;


import com.javacoo.swing.api.data.DataHandler;
import com.javacoo.xkernel.spi.ExtensionLoader;
import com.teamdev.jxbrowser.chromium.BeforeSendHeadersParams;
import com.teamdev.jxbrowser.chromium.DataReceivedParams;
import com.teamdev.jxbrowser.chromium.swing.DefaultNetworkDelegate;

import java.io.UnsupportedEncodingException;

import org.apache.commons.lang3.StringUtils;

/**
 * 网络代理对象
 * <p>说明:</p>
 * <li></li>
 *
 * @author DuanYong
 * @version 1.0
 * @since 2019/8/28 11:20
 */
public class MyNetworkDelegate extends DefaultNetworkDelegate {
    @Override
    public void onBeforeSendHeaders(BeforeSendHeadersParams params) {
        params.getHeadersEx().setHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 UBrowser/6.2.4094.1 Safari/537.36");
        params.getHeadersEx().setHeader("upgrade-insecure-requests","1");
        params.getHeadersEx().setHeader("cache-control","max-age=0");
        params.getHeadersEx().setHeader("accept-encoding","gzip, deflate, sdch, br");
        params.getHeadersEx().setHeader("accept-language","zh-CN,zh;q=0.8");
        params.getHeadersEx().setHeader("upgrade-insecure-requests","1");
        params.getHeadersEx().removeHeader("Connection");
    }
}
