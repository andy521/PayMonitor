package com.javacoo.swing.core.config;

import com.javacoo.swing.api.config.TableConfig;

/**
 * 默认table
 * <p>说明:</p>
 * <li></li>
 *
 * @author duanyong@jccfc.com
 * @date 2020/9/6 22:10
 */
public class DefaultTableConfig implements TableConfig {
    /** 列表标题对象数组 */
    private static final Object[] COLUMN_NAMES = {"入账时间", "支付宝交易号|流水号","商户订单号","对方信息","账务类型", "收支金额（元）", "账户余额（元）", "备注"};
    /** 列表宽度数组 */
    private static final int[] COLUMN_WIDTHS = {135, 180, 180, 150, 80, 100, 100,300};
    /**
     * 表格列宽度数组
     * <p>说明:</p>
     * <li></li>
     *
     * @author duanyong
     * @date 2020/9/6 22:04
     */
    @Override
    public int[] getColumnWidths() {
        return COLUMN_WIDTHS;
    }

    /**
     * 表格标题数组
     * <p>说明:</p>
     * <li></li>
     *
     * @author duanyong
     * @date 2020/9/6 22:08
     */
    @Override
    public Object[] getColumnTitles() {
        return COLUMN_NAMES;
    }
}
