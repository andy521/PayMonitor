package com.javacoo.swing.core.config;

import com.javacoo.swing.api.config.TableConfig;

/**
 * 我的账单
 * <p>说明:</p>
 * <li></li>
 *
 * @author duanyong@jccfc.com
 * @date 2020/9/6 22:10
 */
public class MyBillRecordTableConfig implements TableConfig {
    /** 列表标题对象数组 */
    private static final Object[] COLUMN_NAMES = {"入账时间", "支付宝交易号|流水号","收支金额（元）","备注"};
    /** 列表宽度数组 */
    private static final int[] COLUMN_WIDTHS = {100, 230,60,500};
    /**
     * 表格列宽度数组
     * <p>说明:</p>
     * <li></li>
     *
     * @author duanyong
     * @date 2020/9/6 22:04
     */
    @Override
    public int[] getColumnWidths() {
        return COLUMN_WIDTHS;
    }

    /**
     * 表格标题数组
     * <p>说明:</p>
     * <li></li>
     *
     * @author duanyong
     * @date 2020/9/6 22:08
     */
    @Override
    public Object[] getColumnTitles() {
        return COLUMN_NAMES;
    }
}
