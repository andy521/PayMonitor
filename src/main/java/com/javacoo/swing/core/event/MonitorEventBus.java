package com.javacoo.swing.core.event;

import com.google.common.eventbus.AsyncEventBus;
import java.util.concurrent.Executors;

/**
 * 事件处理器
 * <p>说明:</p>
 * <li>基于google eventbus</li>
 *
 * @Author DuanYong
 * @Since 2019/8/31 19:59
 * @Version 1.0
 */
public class MonitorEventBus {
    private static AsyncEventBus eventBus = null;
    private static class MonitorEventBusHolder {
        /**
         * 静态初始化器，由JVM来保证线程安全
         */
        private static MonitorEventBus instance = new MonitorEventBus();
    }
    private MonitorEventBus() {
        this.eventBus = new AsyncEventBus(Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors()));
    }
    public static MonitorEventBus getInstance() {
        return MonitorEventBusHolder.instance;
    }

    public void register(Object object){
        eventBus.register(object);
    }

    /**
     * 执行事件
     * @param object
     */
    public void post(Object object){
        eventBus.post(object);
    }

    /**
     * 卸载事件
     * @param object
     */
    public void unRegister(Object object){
        eventBus.unregister(object);
    }
}
